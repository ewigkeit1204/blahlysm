FROM maven:3-jdk-8-alpine AS build-env
ADD . /work
WORKDIR /work
RUN mvn package

FROM openjdk:8-jre-alpine
COPY --from=build-env /work/target/blahlysm-0.0.2-SNAPSHOT.jar /blahlysm.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/blahlysm.jar"]
