# Blahlysm

An automatic submit performance tool.

## Prerequisites

- Docker or Podman

##How to build

`$ docker build -t jp.ewigkeit/blahlysm .`

or

`$ podman build -t jp.ewigkeit/blahlysm .`

> If you want to build with the above method, you need Dokcer 17.05 or higher.

## How to run

`$ docker run -v /path/to/application.properties:/application.properties jp.ewigkeit/blahlysm`

or

`$ podman run -v /path/to/application.properties:/application.properties jp.ewigkeit/blahlysm`

## Settings

- `blahlysm.loginId`

- `blahlysm.password`

- `blahlysm.performance` (choose: `Excellent`, `WellDone`, `NotSoGood`)

- `blahlysm.randomPerformance` (default: `false`)

