/*
 * Copyright 2018 Keisuke Kamada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.ewigkeit.blahlysm;

/**
 * @author user
 *
 */
public enum BlahlysmPerformance {

    Excellent(1), WellDone(2), NotSoGood(3);

    int index;

    BlahlysmPerformance(int index) {
        this.index = index;
    }

    public int getIndex() {
        return this.index;
    }

}
