/*
 * Copyright 2018 Keisuke Kamada
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.ewigkeit.blahlysm;

import java.security.SecureRandom;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author user
 */
@Service
public class BlahlysmService {

    private static final String LOGIN_URL = "https://www.willysm.com/login";

    protected WebDriver driver;

    private BlahlysmProperties properties;

    @Autowired
    public BlahlysmService(BlahlysmProperties properties) {
        this.properties = properties;
        this.driver = new HtmlUnitDriver(true);
    }

    public void submitPerformance() {
        driver.get(LOGIN_URL);
        driver.findElement(By.id("authentication_form_login_id")).sendKeys(properties.getLoginId());
        driver.findElement(By.id("authentication_form_password")).sendKeys(properties.getPassword());
        driver.findElement(By.name("commit")).click();

        BlahlysmPerformance performance;

        if (properties.isRandomPerformance()) {
            Random random = new SecureRandom();
            performance = BlahlysmPerformance.values()[random.nextInt(BlahlysmPerformance.values().length)];
        } else {
            performance = properties.getPerformance();
        }

        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(
                By.xpath("//*[@id=\"performance_input_area\"]/table/tbody/tr/td[" + performance.getIndex() + "]/div")));
        driver.findElement(
                By.xpath("//*[@id=\"performance_input_area\"]/table/tbody/tr/td[" + performance.getIndex() + "]/div"))
                .click();

        driver.quit();
    }

}
