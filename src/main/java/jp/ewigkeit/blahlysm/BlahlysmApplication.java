package jp.ewigkeit.blahlysm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlahlysmApplication implements CommandLineRunner {

    @Autowired
    private BlahlysmService service;

    public static void main(String[] args) {
        SpringApplication.run(BlahlysmApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        service.submitPerformance();
    }

}
